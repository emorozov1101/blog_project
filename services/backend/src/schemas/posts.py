from typing import Optional

from pydantic import BaseModel
from tortoise.contrib.pydantic.creator import pydantic_model_creator

from src.database.models import Post


PostInSchema = pydantic_model_creator(Post, name="PostIn", exclude=("author",), exclude_readonly=True)
PostOutSchema = pydantic_model_creator(
    Post, name="PostIn", exclude=("modified_at", "author.password", "author.created_at", "author.modified_at")
)


class UpdatePost(BaseModel):
    title: Optional[str]
    content: Optional[str]
