from tortoise.contrib.pydantic.creator import pydantic_model_creator

from src.database.models import User


UserInSchema = pydantic_model_creator(User, name="UserIn", exclude_readonly=True)
UserOutSchema = pydantic_model_creator(
    # User, name="UserOut", exclude=("first_name", "last_name", "password", "created_at", "modified_at")
    User, name="UserOut", exclude=("created_at", "modified_at")
)
UserDatabaseSchema = pydantic_model_creator(User, name="User", exclude=("created_at", "modified_at"))
