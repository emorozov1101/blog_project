from os import getenv

TORTOISE_ORM = {
    "connections": {"default": getenv("DATABASE_URL")},
    "apps": {"models": {"models": ["src.database.models", "aerich.models"], "default_connection": "default"}},
}
